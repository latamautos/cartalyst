# Support

[![Build Status](http://ci.cartalyst.com/build-status/svg/18)](http://ci.cartalyst.com/build-status/view/18)

A support package that provides flexible and reusable helper methods and traits for commonly used functionalities.

## Package Story

Package history and capabilities.

#### xx-xx-14 - v1.1.0

- `Mailer` A Mailer class that implements the `Illuminate\Mail\Mailer` with lots of helper methods.
- `Validator` A Validation class that allows you to define different rules for different scenarios throughout your application.

#### 07-Aug-14 - v1.0.0

- `EventTrait` Common methods and properties for dispatching events.
- `RepositoryTrait` Common methods and properties for use across repositories.

## Installation

Support is installable with Composer. Read further information on how to install.

[Installation Guide](https://cartalyst.com/manual/support/1.1#installation)

## Documentation

Refer to the following guide on how to use the Support package.

[Documentation](https://cartalyst.com/manual/support/1.1)

## Testing

```bash
$ phpunit
```

## Versioning

We version under the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

## Contributing

Please see [contributing.md](contributing.md) for details.

## Support

Have a bug? Please create an [issue](https://github.com/cartalyst/support/issues) here on GitHub that conforms with [necolas's guidelines](https://github.com/necolas/issue-guidelines).

Follow us on Twitter, [@cartalyst](http://twitter.com/cartalyst).

Join us for a chat on IRC.

Server: irc.freenode.net
Channel: #cartalyst

Email: help@cartalyst.com
