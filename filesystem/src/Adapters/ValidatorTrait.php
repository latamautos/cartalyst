<?php namespace Cartalyst\Filesystem\Adapters;
/**
 * Part of the Filesystem package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Filesystem
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

trait ValidatorTrait {

	/**
	 * Validates required parameters.
	 *
	 * @param  array  $config
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	protected function validate($config)
	{
		foreach ($this->required as $key)
		{
			if ( ! array_get($config, $key))
			{
				throw new \InvalidArgumentException("$key is required.");
			}
		}
	}

}
