<?php namespace Cartalyst\SentinelSocial\RequestProviders;
/**
 * Part of the Sentinel Social package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Sentinel Social
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

class NativeRequestProvider implements RequestProviderInterface {

	/**
	 * {@inheritDoc}
	 */
	public function getOAuth1TemporaryCredentialsIdentifier()
	{
		return isset($_GET['oauth_token']) ? $_GET['oauth_token'] : null;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOAuth1Verifier()
	{
		return isset($_GET['oauth_verifier']) ? $_GET['oauth_verifier'] : null;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOAuth2Code()
	{
		return isset($_GET['code']) ? $_GET['code'] : null;
	}

}
